import sys
import matplotlib.pyplot as plt
import numpy as np



# Read a file from args

# The file looks like this:
# Experiment 1000:
# Compression type: None
# Total grid logical size: 108.0
# Number of iterations: 80
# Total time: 0.15427
# GB processed per second: 109.38614118104623
# Subgrid number x: 1
# Experiment 1001:
# Compression type: None
# Total grid logical size: 864.0
# Number of iterations: 160
# Total time: 1.990975
# GB processed per second: 135.61194891949924
# Subgrid number x: 1
# Experiment 1002:
# .....

# This script will place data points on a graph, where the Y axis is the GB processed per second, and the X axis is the total size of the logical subgrid
# There are different data points types:
# - None_bounceback: no compression, normal bounceback
# - None_nobounceback: no compression, no bounceback (useless, but demonstrates that without the bounceback bottleneck, the step kernel is near the peak performance)
# - Local_wavelets: local wavelets compression, normal bounceback
#    4 subtypes in Local_wavelets: 1x4x1 subgrids, 2x8x2 subgrids, 3x12x3 subgrids, 4x16x4 subgrids
# - Global_wavelets: global wavelets compression, normal bounceback
# To be visually clear, these datapoints should be clearly distinguishable from one another.
# I propose:
# - None_bounceback: cross +
# - None_nobounceback: cross x
# - Local_wavelets: a geometrical shape with N side, where N is 1 for 1x4x1 for example.
#    This makes: a point for 1x4x1, a - line for 2x8x2, a triangle for 3x12x3, a square for 4x16x4
# - Global_wavelets: a circle

# What we expect to see:
# Before the max GPU size (the logical subgrid size), the "None" kernels (no compression) are faster than the wavelets kernels
# After the max GPU size, the "None" kernels cannot work, and the kernels with compression become a reasonable option
# The global wavelets are always slower

# Read the file
if len(sys.argv) < 2:
    print('Usage: python3 plot_threshold_experiments.py <file_P100> <file_V100> <file_A100>')
    sys.exit(1)

filenames = []
for i in range(1, len(sys.argv)):
    filenames.append(sys.argv[i])

# Array of results (datapoints), will look like this: [{'type': 'None_bounceback', 'subgrid_num_x': -1, 'grid_size': 108.0, 'gb_processed_per_second': 109.38614118104623}, ...]
# subgrid_num is only set for Local_wavelets
results_per_arch = []

filename_id = 0
# Read the file
for filename in filenames:
    with open(filename, 'r') as f:
        lines = f.readlines()

        results_arch = []

        # Iterate over the lines
        for line in lines:
            # If the line starts with "Experiment ", then we found the line we want
            if line.startswith('Experiment '):
                # Split the line by spaces
                line_split = line.split(' ')
                # The last element of the split line is the experiment number
                experiment_number = int(line_split[-1][:-2])

                # The next lines give the info we need
                compression_type = lines[lines.index(line) + 1].split(' ')[-1][:-1]
                grid_size = float(lines[lines.index(line) + 2].split(' ')[-1])
                num_iterations = int(lines[lines.index(line) + 3].split(' ')[-1])
                total_time = float(lines[lines.index(line) + 4].split(' ')[-1])
                gb_processed_per_second = float(lines[lines.index(line) + 5].split(' ')[-1])
                subgrid_num_x = int(lines[lines.index(line) + 6].split(' ')[-1])

                if experiment_number < 1500 and compression_type == 'None':
                    compression_type = 'None_bounceback'
                elif compression_type == 'None':
                    compression_type = 'None_nobounceback'
                
                if compression_type == 'Shared':
                    compression_type = 'Local_wavelets'
                elif compression_type == 'Global':
                    compression_type = 'Global_wavelets'
                elif compression_type == 'None, but subgrids':
                    compression_type = 'subgrids'

                # Add the result to the list
                results_arch.append({'experiment_number': experiment_number, 'compression_type': compression_type, 'grid_size': grid_size, 'num_iterations': num_iterations, 'total_time': total_time, 'gb_processed_per_second': gb_processed_per_second, 'subgrid_num_x': subgrid_num_x})
    results_per_arch.append(results_arch)
    filename_id += 1

print(results_per_arch)

# Plot the results with pyplot
# Colorblind friendly colors
# CB_color_cycle = ['#377eb8', '#ff7f00', '#4daf4a',
#                   '#f781bf', '#a65628', '#984ea3',
#                   '#999999', '#e41a1c', '#dede00']

arch_colors = ['#377eb8', '#ff7f00', '#4daf4a'] # One color per architecture
GPU_upper_limit = [732, 900, 1555] # Max GPU bandwidth for each architecture (GB/s)
GPU_right_limit = [16*1024-512, 16*1024, 40*1024] # Max GPU size for each architecture (GB)

# Increase the default size of the figure
plt.rcParams["figure.figsize"] = (16, 8)

# Create the figure
fig, ax = plt.subplots()

# Set the title
ax.set_title('GB processed per second depending on the used method and the grid size', fontsize=16)
# ax1.title.set_fontsize(16)
ax.set_xlabel('Grid size', fontsize=12)
ax.set_ylabel('GB processed per second', fontsize=12)

# Use log scale
ax.set_xscale('log')
ax.set_yscale('log')

max_y_graph = np.max(GPU_upper_limit) * 1.2
max_x_graph=1 # GB
while max_x_graph < np.max([result['grid_size'] for result in results_arch for results_arch in results_per_arch]):
    max_x_graph *= 2
max_x_graph *= 1.2

# Custom ticks for the X axis, we want something like:
# 128MB   256MB    512MB    1GB    2GB    ... max_x_graph GB

ax.set_xticks([]) # Remove the old ticks
ax.xaxis.set_minor_locator(plt.NullLocator()) # Remove the old minor ticks

# Add the new ticks
# First the MB ticks
ax.set_xticks([128, 256, 512])
ax.set_xticklabels(['128MB', '256MB', '512MB'])
# Then the GB ticks
current_tick = 1024
while current_tick < max_x_graph:
    # Append the new tick
    ax.set_xticks(np.concatenate((ax.get_xticks(), [current_tick])))
    # ax.set_xticklabels(np.concatenate((ax.get_xticklabels(), [f'{current_tick}GB']))) # ValueError: The number of FixedLocator locations (4), usually from a call to set_ticks, does not match the number of ticklabels (5)
    newlabels = ax.get_xticklabels()
    print(newlabels)
    if current_tick < 1024*1024:
        newlabels[-1] = f'{int(current_tick/1024)}GB'
    else:
        newlabels[-1] = f'{int(current_tick/1024/1024)}TB'
    ax.set_xticklabels(newlabels)
    current_tick *= 2

# The Y axis must start at 0
ax.set_ylim(bottom=5, top=max_y_graph)

# Write the y ticks without scientific notation
# ax.get_yaxis().get_major_formatter().set_useOffset(False) # AttributeError: 'LogFormatterSciNotation' object has no attribute 'set_useOffset'
# Use this instead:
ax.yaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: '{:.16g}'.format(x)))

# Add a y tick at 32 (PCIe gen 3 limit) and 300 (SXM2 limit)
ax.set_yticks([32, 100, 300, 1000])

# Plot the data points
# The color of the data points depends on the architecture (arch_colors array)
arch_id = 0
for results_arch in results_per_arch:
    for result in results_arch:
        if result['compression_type'] == 'None_bounceback':
            ax.plot(result['grid_size'], result['gb_processed_per_second'], 'v', color=arch_colors[arch_id], markersize=10)
        elif result['compression_type'] == 'None_nobounceback':
            ax.plot(result['grid_size'], result['gb_processed_per_second'], '^', color=arch_colors[arch_id], markersize=10)
            #print(f'ignore None_nobounceback{result["grid_size"]}')
        elif result['compression_type'] == 'Local_wavelets':
            # # ax.plot(result['grid_size'], result['gb_processed_per_second'], 'o', color=arch_colors[arch_id])
            # # If subgrid_num_x is 1, plot a point, if 2, plot a line, if 3, plot a triangle, if 4, plot a square
            # if result['subgrid_num_x'] == 1:
            #     ax.plot(result['grid_size'], result['gb_processed_per_second'], '.', color=arch_colors[arch_id])
            # elif result['subgrid_num_x'] == 2:
            #     ax.plot([result['grid_size'], result['grid_size']], [result['gb_processed_per_second'], result['gb_processed_per_second']], '_', color=arch_colors[arch_id], markersize=12)
            # elif result['subgrid_num_x'] == 3:
            #     ax.plot([result['grid_size'], result['grid_size']], [result['gb_processed_per_second'], result['gb_processed_per_second']], '^', color=arch_colors[arch_id], markersize=12)
            # elif result['subgrid_num_x'] == 4:
            #     ax.plot([result['grid_size'], result['grid_size']], [result['gb_processed_per_second'], result['gb_processed_per_second']], 's', color=arch_colors[arch_id], markersize=12)
            # Just use a single pattern for all these:
            ax.plot(result['grid_size'], result['gb_processed_per_second'], 'X', color=arch_colors[arch_id], markersize=10)
        elif result['compression_type'] == 'Global_wavelets':
            ax.plot(result['grid_size'], result['gb_processed_per_second'], '*', color=arch_colors[arch_id], markersize=12)
            # print(f'ignore Global_wavelets{result["grid_size"]}')
        elif result['compression_type'] == 'subgrids':
            ax.plot(result['grid_size'], result['gb_processed_per_second'], 'D', color=arch_colors[arch_id], markersize=8)
    print(f'arch_id: {arch_id}')
    arch_id += 1

# Plot the GPU limits with dotted lines of the proper color
arch_id = 0
for results_arch in results_per_arch:
    ax.plot([GPU_right_limit[arch_id], GPU_right_limit[arch_id]], [0, GPU_upper_limit[arch_id]], '--', color=arch_colors[arch_id])
    ax.plot([0, GPU_right_limit[arch_id]], [GPU_upper_limit[arch_id], GPU_upper_limit[arch_id]], '--', color=arch_colors[arch_id])
    arch_id += 1

# Plot some GPU connection lines with a dotted line (but a different pattern)

# The GPU limit
ax.plot(0, 0, '--', color='gray', label='GPU limit (without compression)')
# PCIE gen 3 is at 32GB/s
ax.plot([0, max_x_graph], [32, 32], ':', color='gray', label='PCIe gen 3 limit')
# SXM2 is at 300GB/s
ax.plot([0, max_x_graph], [300, 300], '-.', color='gray', label='SXM2 limit (nvlink)')

# Display the labels on the right side
ax.text(max_x_graph, 32, 'PCIe gen 3 limit', fontsize=8, verticalalignment='bottom', horizontalalignment='right')
ax.text(max_x_graph, 300, 'SXM2 limit', fontsize=8, verticalalignment='bottom', horizontalalignment='right')

# Add a legend
# First, the (pyplot) symbols (markers): 1, 2, ., _, ^, s, * (in black, because the color depends on the architecture)
# Second the colors (and the corresponding architecture)

# The markers
ax.plot(0, 0, '^', color='gray', markersize=10, label='No subgrids, no compression, no bounceback')
ax.plot(0, 0, 'v', color='gray', markersize=10, label='No subgrids, no compression, bounceback')
ax.plot(0, 0, 'D', color='gray', markersize=8, label='Subgrids, no compression, bounceback')
ax.plot(0, 0, 'X', color='gray', markersize=10, label='Subgrids, block compression, bounceback')
ax.plot(0, 0, '*', color='gray', markersize=12, label='Subgrids, global compression, bounceback')

# The colors
ax.plot(0, 0, 's', color=arch_colors[2], markersize=12, label='Color of A100 data')
ax.plot(0, 0, 's', color=arch_colors[1], markersize=12, label='Color of V100 data')
ax.plot(0, 0, 's', color=arch_colors[0], markersize=12, label='Color of P100 data')

# Add the legend
ax.legend(loc='center left', bbox_to_anchor=(1, 0.75))

# Displace the plot to be able to see the legend
plt.subplots_adjust(left=0.1, right=0.74, top=0.9, bottom=0.1)

plt.show()
