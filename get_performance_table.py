import sys
import os
import gzip
import numpy as np
import time

# We get an output like this:

# This python script (get_performance_table.py) will look at the folder given as argument, and will print a nice summary of the results.
# The summary is output in a latex table format.

# For example, if we run it like this:
# python3 summary_loss_threshold_experiment.py 2591743
# It will analyize the following files:
# 2591743/experiment_1000/output.txt
# 2591743/experiment_1001/output.txt
# ...
# 2591743/experiment_2000/output.txt
# 2591743/experiment_2001/output.txt
# ...
# 2591743/experiment_3000/output.txt
# 2591743/experiment_3001/output.txt
# ...
# 2591743/experiment_3999/output.txt (if exists)

# And then parse their results.

# The lines of interest in the output are the end of the file, and it looks like this:
# Total time spent performing the numerical scheme: 0.779574 s (87.479004%)
# Total time spent compressing: 0.000000 s (0.000000%)
# Total time spent decompressing: 0.000000 s (0.000000%)
# Total time spent synchronizing: 0.111581 s (12.520996%)
# Total time: 0.891155 s


# Parse from stdarg the filename
if len(sys.argv) < 2:
    print('Usage: python3 summary_loss_threshold_experiment.py <run_dir_gpu1> <run_dir_gpu2> <run_dir_gpu3>')
    sys.exit(1)

GPUs = ['A100', 'V100', 'P100']

directories = [arg for arg in sys.argv[1:]]

experiments_data=[]

gpu_id=0
for directory in directories:
    for i in range(1000,4999):
        filename = f'{directory}/experiment_{i}/output.txt'

        # Verify that the file exists
        if not os.path.exists(filename):
            continue

        # Read the output file to get the threshold used for the experiment and the average compression ratio
        with open(filename, 'r') as f:
            lines = f.readlines()

            # If between 1000 and 1999 included, the compression type is "None"
            # If between 2000 and 2999 included, the compression type is "Shared"
            # If between 3000 and 3999 included, the compression type is "Global"
            # If between 4000 and 4999 included, the compression type is "None, but subgrids"

            # The line of the output that gives the Total grid logical size looks like this: "Total grid logical size = 108.000000 MB"
            # The one that gives the number of iterations looks like this: "Performed 160 iterations"
            # The one that gives the total time looks like this: "Total time: 3.912142 s"
            # The GB processed per second is computed like this: (grid_size/1024)*num_iterations/total_time*2 (the *2 is because we have two grids)
            # subgrid_num_x: "Subgrid number per dimension = 4 16 4" means 4x16x4, so 4 is the subgrid_num_x

            current_experiment_data = {}
            current_experiment_data['experiment_id'] = i
            current_experiment_data['percentage_numerical_scheme'] = 0.0
            current_experiment_data['percentage_compression'] = 0.0
            current_experiment_data['percentage_decompression'] = 0.0
            current_experiment_data['percentage_synchronizing'] = 0.0
            current_experiment_data['total_time'] = 0.0
            current_experiment_data['compression_ratio'] = 1.0
            
            # Iterate over the lines:
            for line in lines:
                # end of iteration 0(t=0.0078): compression rate (real)=/738.005519(60044471424/81360464)
                if line.startswith('end of iteration 0(') and 'compression rate' in line:
                    current_experiment_data['compression_ratio'] = float(line.split('=')[2].split('(')[0][1:])
                elif line.startswith('Total time spent performing the numerical scheme:'):
                    current_experiment_data['percentage_numerical_scheme'] = float(line.split(' ')[-1].split('%')[0][1:])
                elif line.startswith('Total time spent compressing:'):
                    current_experiment_data['percentage_compression'] = float(line.split(' ')[-1].split('%')[0][1:])
                elif line.startswith('Total time spent decompressing:'):
                    current_experiment_data['percentage_decompression'] = float(line.split(' ')[-1].split('%')[0][1:])
                elif line.startswith('Total time spent synchronizing:'):
                    current_experiment_data['percentage_synchronizing'] = float(line.split(' ')[-1].split('%')[0][1:])
                elif line.startswith('Total time:'):
                    current_experiment_data['total_time'] = float(line.split(' ')[-2])

        # If the sum of the times is not close to 100%, the experiment is invalid
        if not np.isclose(current_experiment_data['percentage_numerical_scheme'] + current_experiment_data['percentage_compression'] + current_experiment_data['percentage_decompression'] + current_experiment_data['percentage_synchronizing'], 100.0):
            continue

        current_experiment_data['experiment_type'] = 'Unknown'
        
        if i >= 1000 and i <= 1999:
            current_experiment_data['experiment_type']='No subgrids, no compression'
        elif i >= 2000 and i <= 2999:
            current_experiment_data['experiment_type']='Subgrids, block compression'
        elif i >= 3000 and i <= 3999:
            current_experiment_data['experiment_type']='Subgrids, global compression'
        elif i >= 4000 and i <= 4999:
            current_experiment_data['experiment_type']='Subgrids, no compression'
        else:
            current_experiment_data['experiment_type']='Unknown'
        
        current_experiment_data['GPU'] = GPUs[gpu_id]

        experiments_data.append(current_experiment_data)
    gpu_id+=1

# Compute the average percentage of each type of operation
    
average_percentage_numerical_scheme_no_subgrids_no_compression = np.mean([experiment['percentage_numerical_scheme'] for experiment in experiments_data if experiment['experiment_type']=='No subgrids, no compression'])
average_percentage_compression_no_subgrids_no_compression = np.mean([experiment['percentage_compression'] for experiment in experiments_data if experiment['experiment_type']=='No subgrids, no compression'])
average_percentage_decompression_no_subgrids_no_compression = np.mean([experiment['percentage_decompression'] for experiment in experiments_data if experiment['experiment_type']=='No subgrids, no compression'])
average_percentage_synchronizing_no_subgrids_no_compression = np.mean([experiment['percentage_synchronizing'] for experiment in experiments_data if experiment['experiment_type']=='No subgrids, no compression'])

average_percentage_numerical_scheme_subgrids_block_compression = np.mean([experiment['percentage_numerical_scheme'] for experiment in experiments_data if experiment['experiment_type']=='Subgrids, block compression'])
average_percentage_compression_subgrids_block_compression = np.mean([experiment['percentage_compression'] for experiment in experiments_data if experiment['experiment_type']=='Subgrids, block compression'])
average_percentage_decompression_subgrids_block_compression = np.mean([experiment['percentage_decompression'] for experiment in experiments_data if experiment['experiment_type']=='Subgrids, block compression'])
average_percentage_synchronizing_subgrids_block_compression = np.mean([experiment['percentage_synchronizing'] for experiment in experiments_data if experiment['experiment_type']=='Subgrids, block compression'])

average_percentage_numerical_scheme_subgrids_global_compression = np.mean([experiment['percentage_numerical_scheme'] for experiment in experiments_data if experiment['experiment_type']=='Subgrids, global compression'])
average_percentage_compression_subgrids_global_compression = np.mean([experiment['percentage_compression'] for experiment in experiments_data if experiment['experiment_type']=='Subgrids, global compression'])
average_percentage_decompression_subgrids_global_compression = np.mean([experiment['percentage_decompression'] for experiment in experiments_data if experiment['experiment_type']=='Subgrids, global compression'])
average_percentage_synchronizing_subgrids_global_compression = np.mean([experiment['percentage_synchronizing'] for experiment in experiments_data if experiment['experiment_type']=='Subgrids, global compression'])

average_percentage_numerical_scheme_subgrids_no_compression = np.mean([experiment['percentage_numerical_scheme'] for experiment in experiments_data if experiment['experiment_type']=='Subgrids, no compression'])
average_percentage_compression_subgrids_no_compression = np.mean([experiment['percentage_compression'] for experiment in experiments_data if experiment['experiment_type']=='Subgrids, no compression'])
average_percentage_decompression_subgrids_no_compression = np.mean([experiment['percentage_decompression'] for experiment in experiments_data if experiment['experiment_type']=='Subgrids, no compression'])
average_percentage_synchronizing_subgrids_no_compression = np.mean([experiment['percentage_synchronizing'] for experiment in experiments_data if experiment['experiment_type']=='Subgrids, no compression'])

# Ensure that the sum of each type of operation is 100% within a type of experiment
assert np.isclose(average_percentage_numerical_scheme_no_subgrids_no_compression + average_percentage_compression_no_subgrids_no_compression + average_percentage_decompression_no_subgrids_no_compression + average_percentage_synchronizing_no_subgrids_no_compression, 100.0)
assert np.isclose(average_percentage_numerical_scheme_subgrids_block_compression + average_percentage_compression_subgrids_block_compression + average_percentage_decompression_subgrids_block_compression + average_percentage_synchronizing_subgrids_block_compression, 100.0)
assert np.isclose(average_percentage_numerical_scheme_subgrids_global_compression + average_percentage_compression_subgrids_global_compression + average_percentage_decompression_subgrids_global_compression + average_percentage_synchronizing_subgrids_global_compression, 100.0)
assert np.isclose(average_percentage_numerical_scheme_subgrids_no_compression + average_percentage_compression_subgrids_no_compression + average_percentage_decompression_subgrids_no_compression + average_percentage_synchronizing_subgrids_no_compression, 100.0)

# # Print the results in a nice table
# print("\\begin{tabular}{|c|c|c|c|c|}")
# print('\\hline')
# print('Compression type & Numerical scheme & Compression & Decompression & Synchronizing \\\\')
# print('\\hline')
# print(f'No subgrids, no compression & {average_percentage_numerical_scheme_no_subgrids_no_compression:.2f}\\% & {average_percentage_compression_no_subgrids_no_compression:.2f}\\% & {average_percentage_decompression_no_subgrids_no_compression:.2f}\\% & {average_percentage_synchronizing_no_subgrids_no_compression:.2f}\\% \\\\')
# print(f'Subgrids, no compression & {average_percentage_numerical_scheme_subgrids_no_compression:.2f}\\% & {average_percentage_compression_subgrids_no_compression:.2f}\\% & {average_percentage_decompression_subgrids_no_compression:.2f}\\% & {average_percentage_synchronizing_subgrids_no_compression:.2f}\\% \\\\')
# print(f'Subgrids, block compression & {average_percentage_numerical_scheme_subgrids_block_compression:.2f}\\% & {average_percentage_compression_subgrids_block_compression:.2f}\\% & {average_percentage_decompression_subgrids_block_compression:.2f}\\% & {average_percentage_synchronizing_subgrids_block_compression:.2f}\\% \\\\')
# print(f'Subgrids, global compression & {average_percentage_numerical_scheme_subgrids_global_compression:.2f}\\% & {average_percentage_compression_subgrids_global_compression:.2f}\\% & {average_percentage_decompression_subgrids_global_compression:.2f}\\% & {average_percentage_synchronizing_subgrids_global_compression:.2f}\\% \\\\')
# print('\\hline')
# print('\\end{tabular}')

# Use a more generic version that iterates over the GPU type and execution type
print("\\begin{tabular}{|c|c|c|c|c|c|c|}")
print('\\hline')
print('\\multicolumn{2}{|c|}{} & \multicolumn{4}{c|}{Proportion of time spent in the kernels} & \\multicolumn{1}{c|}{Compression ratio} \\\\')
print('\\multicolumn{2}{|c|}{} & \multicolumn{4}{c|}{(in percentage)} & \\multicolumn{1}{c|}{(first time step)} \\\\')
print('\\hline')
print('GPU & Configuration & Numerical scheme & Compression & Decompression & Synchronization & Ratio \\\\')
print('\\hline')
for gpu in GPUs:
    average_percentage_numerical_scheme_no_subgrids_no_compression = {}
    average_percentage_compression_no_subgrids_no_compression = {}
    average_percentage_decompression_no_subgrids_no_compression = {}
    average_percentage_synchronizing_no_subgrids_no_compression = {}

    # iterate over the execution types
    for experiment_type in ['No subgrids, no compression', 'Subgrids, no compression', 'Subgrids, block compression', 'Subgrids, global compression']:
        average_percentage_numerical_scheme_no_subgrids_no_compression[experiment_type] = np.mean([experiment['percentage_numerical_scheme'] for experiment in experiments_data if experiment['experiment_type']==experiment_type and experiment['GPU']==gpu])
        average_percentage_compression_no_subgrids_no_compression[experiment_type] = np.mean([experiment['percentage_compression'] for experiment in experiments_data if experiment['experiment_type']==experiment_type and experiment['GPU']==gpu])
        average_percentage_decompression_no_subgrids_no_compression[experiment_type] = np.mean([experiment['percentage_decompression'] for experiment in experiments_data if experiment['experiment_type']==experiment_type and experiment['GPU']==gpu])
        average_percentage_synchronizing_no_subgrids_no_compression[experiment_type] = np.mean([experiment['percentage_synchronizing'] for experiment in experiments_data if experiment['experiment_type']==experiment_type and experiment['GPU']==gpu])
        assert np.isclose(average_percentage_numerical_scheme_no_subgrids_no_compression[experiment_type] + average_percentage_compression_no_subgrids_no_compression[experiment_type] + average_percentage_decompression_no_subgrids_no_compression[experiment_type] + average_percentage_synchronizing_no_subgrids_no_compression[experiment_type], 100.0)
    
    # Get the four compression ratios
    compression_ratios = [1.0, 1.0, 1.0, 1.0]
    compression_ratios[2] = np.mean([experiment['compression_ratio'] for experiment in experiments_data if experiment['experiment_type']=='Subgrids, block compression' and experiment['GPU']==gpu])
    compression_ratios[3] = np.mean([experiment['compression_ratio'] for experiment in experiments_data if experiment['experiment_type']=='Subgrids, global compression' and experiment['GPU']==gpu])

    print(f'{gpu} & No subgrids, no compression & {average_percentage_numerical_scheme_no_subgrids_no_compression["No subgrids, no compression"]:.2f}\\% & {average_percentage_compression_no_subgrids_no_compression["No subgrids, no compression"]:.2f}\\% & {average_percentage_decompression_no_subgrids_no_compression["No subgrids, no compression"]:.2f}\\% & {average_percentage_synchronizing_no_subgrids_no_compression["No subgrids, no compression"]:.2f}\\% & {compression_ratios[0]:.2f} \\\\')
    print(f'{gpu} & Subgrids, no compression & {average_percentage_numerical_scheme_no_subgrids_no_compression["Subgrids, no compression"]:.2f}\\% & {average_percentage_compression_no_subgrids_no_compression["Subgrids, no compression"]:.2f}\\% & {average_percentage_decompression_no_subgrids_no_compression["Subgrids, no compression"]:.2f}\\% & {average_percentage_synchronizing_no_subgrids_no_compression["Subgrids, no compression"]:.2f}\\% & {compression_ratios[1]:.2f} \\\\')
    print(f'{gpu} & Subgrids, block compression & {average_percentage_numerical_scheme_no_subgrids_no_compression["Subgrids, block compression"]:.2f}\\% & {average_percentage_compression_no_subgrids_no_compression["Subgrids, block compression"]:.2f}\\% & {average_percentage_decompression_no_subgrids_no_compression["Subgrids, block compression"]:.2f}\\% & {average_percentage_synchronizing_no_subgrids_no_compression["Subgrids, block compression"]:.2f}\\% & {compression_ratios[2]:.2f} \\\\')
    print(f'{gpu} & Subgrids, global compression & {average_percentage_numerical_scheme_no_subgrids_no_compression["Subgrids, global compression"]:.2f}\\% & {average_percentage_compression_no_subgrids_no_compression["Subgrids, global compression"]:.2f}\\% & {average_percentage_decompression_no_subgrids_no_compression["Subgrids, global compression"]:.2f}\\% & {average_percentage_synchronizing_no_subgrids_no_compression["Subgrids, global compression"]:.2f}\\% & {compression_ratios[3]:.2f} \\\\')
    print('\\hline')
print("\\end{tabular}")
