import sys
import matplotlib.pyplot as plt
import numpy as np



# Read a file from args

# The file looks like this:
# Experiment 10000:
# Total grid logical size: 2916.0
# Number of iterations: 49
# Total time: 1.101623
# GB processed per second: 253.3265123367976
# Average idle time: 0.39825525
# Number of nodes: 4
# Experiment 10001:
# Total grid logical size: 2916.0
# Number of iterations: 49
# Total time: 0.997219
# GB processed per second: 279.8485713769995
# Average idle time: 0.29394825
# Number of nodes: 4
# Experiment 10002:
# Total grid logical size: 2916.0
# Number of iterations: 49
# Total time: 1.06527
# GB processed per second: 261.97143681883466
# Average idle time: 0.361479
# Number of nodes: 4
# .....

# This script will show bars: one bar per number of nodes (1, 2, 4)

# Read the file
if len(sys.argv) < 2:
    print('Usage: python3 plot_threshold_experiments.py <summary_file> [title]')
    sys.exit(1)

# We store the results in a nice structure that will make it easy to put them in a bar plot (a dictionary)

filename = sys.argv[1]

title = 'Execution time for different number of nodes'
if len(sys.argv) > 2:
    title = sys.argv[2]

# Array of results, will look like this: [{'number_of_nodes': 4, 'execution_time': 1.101623, 'idle_time': 0.39825525, 'gb_processed_per_second': 253.3265123367976}, ...]
results = {}

# Read the file
with open(filename, 'r') as f:
    lines = f.readlines()

    # Iterate over the lines
    for line in lines:
        # If the line starts with "Experiment ", then we found the line we want
        if line.startswith('Experiment '):
            # Split the line by spaces
            line_split = line.split(' ')
            # The last element of the split line is the experiment number
            experiment_number = int(line_split[-1][:-2])

            # The next lines are the total grid logical size, the number of iterations, the total time, the GB processed per second, the average idle time and the number of nodes
            total_grid_logical_size = float(lines[lines.index(line) + 1].split(' ')[-1])
            num_iterations = int(lines[lines.index(line) + 2].split(' ')[-1])
            total_time = float(lines[lines.index(line) + 3].split(' ')[-1])
            gb_processed_per_second = float(lines[lines.index(line) + 4].split(' ')[-1])
            average_idle_time = float(lines[lines.index(line) + 5].split(' ')[-1])
            number_of_nodes = int(lines[lines.index(line) + 6].split(' ')[-1])

            # Add the result to the dictionary
            if number_of_nodes not in results:
                results[number_of_nodes] = []

            results[number_of_nodes].append({'experiment_number': experiment_number, 'total_grid_logical_size': total_grid_logical_size, 'num_iterations': num_iterations, 'total_time': total_time, 'gb_processed_per_second': gb_processed_per_second, 'average_idle_time': average_idle_time, 'number_of_nodes': number_of_nodes})

# Order by ascending number of nodes
results = dict(sorted(results.items()))

# Now we can create the bar plots
# X axis: number of nodes (1, 2, 4)
# Y axis: execution time

# The bars will also include the 80% confidence interval (quantiles 0.1 and 0.9)
# They will be shown as a I on the bar (like a 'I')

# The bars are divided in two parts: the busy time (blue) and the idle time (white)
# The lower part of the bar is the busy time, the upper part is the idle time
# The busy part is the total time minus the idle time

# Create the data
average_total_times = []
average_idle_times = []

for number_of_nodes in results:
    total_times = [result['total_time'] for result in results[number_of_nodes]]
    idle_times = [result['average_idle_time'] for result in results[number_of_nodes]]

    average_total_time = np.mean(total_times)
    average_idle_time = np.mean(idle_times)

    average_total_times.append(average_total_time)
    average_idle_times.append(average_idle_time)

print('Average total times:', average_total_times)
print('Average idle times:', average_idle_times)
print('Average busy times:', np.array(average_total_times)-np.array(average_idle_times))

# Create the figure
fig, ax = plt.subplots(figsize=(9, 6))

# Set the title 
ax.set_title(title)
ax.title.set_fontsize(16)
ax.set_xlabel('Number of nodes', fontsize=12)
ax.set_ylabel('Execution time (s)', fontsize=12)

# Create the bars
bar_width = 0.35
bar_positions = np.arange(len(results))

# Create the bars
bars1 = ax.bar(bar_positions, average_total_times, bar_width, label='Idle time', color='white', edgecolor='black')
bars2 = ax.bar(bar_positions, np.array(average_total_times)-np.array(average_idle_times), bar_width, label='Busy time', color='blue', edgecolor='black')

# Add the legend
ax.legend()

# Add the xticks
ax.set_xticks(bar_positions)
ax.set_xticklabels([str(number_of_nodes) for number_of_nodes in results])

plt.show()
