# Research report for paper "On-the-fly lossy compression in GPU fluid simulations using the Discrete Wavelet Transform"

This document is here to provide as much detail as possible about the research conducted for the paper "On-the-fly lossy compression in GPU fluid simulations using the Discrete Wavelet Transform". This document is focus on the benchmarking of the compression algorithm and the results obtained.

## Benchmarking

### Block configuration

The block configuration is the number of threads per block and the number of blocks of the grid.
To assess what block configuration is the best, we ran a D3Q27 simulation with a cylinder obstacle and with compression.
The following simulation configurations were used:

#### Configuration 0:

* Simulation parameters

dt = 0.007576
tmax = 0.050000
dx = dy = dz = 0.007575757802
CFL = 1.000000
simulated space size = 4.000000 x 16.484848 x 4.121212

* Compression parameters

ceil_value = 0.000100
compression_level = 2
LZ4 chunk_size = 0
Compression positioning strategy: contiguous
Lossless compression: CSR

* Grid configuration

Number of subgrids = 32
Grid total logical size = 528 2176 544
Subgrid number per dimension = 2 8 2
Subgrid true size = 266 274 274
Subgrid logical size = 264 272 272

* Data sizes

Total grid logical size = 64374.750000 MB
Subgrid logical size = 2011.710938 MB
Subgrid true size = 2056.868866 MB
Horizontal interfaces size = 325.125000 MB
Vertical interfaces size = 325.125000 MB
Depthwise interfaces size = 325.125000 MB
Required memory if 100% compression = 5089.112732 MB
Left space for compressed subgrids = 33822.887268 MB
Required memory if single grid = 64374.750000 MB

#### Configuration 1:

* Simulation parameters

dt = 0.007782
tmax = 0.050000
dx = dy = dz = 0.007782101166
CFL = 1.000000
simulated space size = 4.000000 x 16.000000 x 4.000000

* Compression parameters

ceil_value = 0.000100
compression_level = 2
LZ4 chunk_size = 0
Compression positioning strategy: contiguous
Lossless compression: CSR

* Grid configuration

Number of subgrids = 32
Grid total logical size = 514 2056 514
Subgrid number per dimension = 2 8 2
Subgrid true size = 259 259 259
Subgrid logical size = 257 257 257

* Data sizes

Total grid logical size = 55946.534546 MB
Subgrid logical size = 1748.329205 MB
Subgrid true size = 1789.464695 MB
Horizontal interfaces size = 290.254395 MB
Vertical interfaces size = 290.254395 MB
Depthwise interfaces size = 290.254395 MB
Required memory if 100% compression = 4449.692574 MB
Left space for compressed subgrids = 34462.307426 MB
Required memory if single grid = 55946.534546 MB

#### Explanation

The configuration 0 used the improved wavelet version that performs the DWT within the shared memory.
The configutation 1 used the fast wavelet algorithm on the global memory and uses cusparse for the CSR compression.
The grid sizes are slightly different because the fast wavelet algorithm requires a grid size that is a power of 2 plus one.
Only 4 iterations are performed, because this experiment only aims to assess the block configuration.

The script used to run the experiments is `script_used_for_benchmark.sh`.

#### Results

The results are shown in the files: block_config_GPU_WTYPE.csv, where GPU is a100, p100 or v100 and WTYPE is the wavelet type: shared (experiment 0) or global (experiment 1).
What we want to know is the best block configuration for each GPU and wavelet type.
By looking at the csv files we define the best configurations:

##### a100

Shared memory wavelet:

Step: 2048 blocks, 512 threads per block
Interface to subgrid: 512 blocks, 64 threads per block
Subgrid to interface: 512 blocks, 64 threads per block
Compress shared: 2048 blocks, 384 threads per block
Decompress shared: 2048 blocks, 128 threads per block

Global memory wavelet:

Step: 2048 blocks, 512 threads per block
Interface to subgrid: 512 blocks, 64 threads per block
Subgrid to interface: 512 blocks, 64 threads per block
Compress global X: 2048 blocks, 64 threads per block
Decompress global X: 2048 blocks, 64 threads per block
Compress global YZ: 2048 blocks, 384 threads per block
Decompress global YZ: 2048 blocks, 64 threads per block

##### p100

Shared memory wavelet:

Step: 2048 blocks, 512 threads per block
Interface to subgrid: 256 blocks, 32 threads per block
Subgrid to interface: 256 blocks, 32 threads per block
Compress shared: 2048 blocks, 512 threads per block
Decompress shared: 2048 blocks, 512 threads per block

Global memory wavelet:

Step: 2048 blocks, 512 threads per block
Interface to subgrid: 256 blocks, 32 threads per block
Subgrid to interface: 256 blocks, 32 threads per block
Compress global X: 2048 blocks, 64 threads per block
Decompress global X: 1024 blocks, 64 threads per block
Compress global YZ: 2048 blocks, 384 threads per block
Decompress global YZ: 2048 blocks, 384 threads per block

##### v100

Shared memory wavelet:

Step: 2048 blocks, 384 threads per block
Interface to subgrid: 256 blocks, 32 threads per block
Subgrid to interface: 256 blocks, 32 threads per block
Compress shared: 2048 blocks, 512 threads per block
Decompress shared: 256 blocks, 384 threads per block

Global memory wavelet:

Step: 2048 blocks, 512 threads per block
Interface to subgrid: 256 blocks, 32 threads per block
Subgrid to interface: 256 blocks, 32 threads per block
Compress global X: 2048 blocks, 64 threads per block
Decompress global X: 1024 blocks, 64 threads per block
Compress global YZ: 2048 blocks, 384 threads per block
Decompress global YZ: 2048 blocks, 384 threads per block

#### Interpretation

In general, the step kernel is more efficient when using a large number of both blocks and threads per block.
It, therefore, makes sense to use the maximum number of blocks and threads per block that the GPU allows.

The interface to subgrid and subgrid to interface kernels are more efficient when using a small number of blocks and threads per block.
This is not surprising, since these kernels perform bad memory accesses, as they must access non-contiguous memory locations.
The a100 GPU is an exception, as 512/64 is significantly faster than 256/32. This exception is worth having a special case in the code.

The shared compress and shared decompress kernels are generally more efficient when using a large number of blocks and threads per block.
The default option should, therefore, be 2048/512.
There are some exceptions that are worth having special cases in the code:
- 2048/128 is significantly better for shared decompression on a100
- There seems to be a benefit to having less blocks on the shared decompression on v100, 256/384 can be a special case

The global compress and global decompress kernels are generally more efficient when using a large number of blocks and threads per block.
The YZ compress and decompress kernels always operate on 32 threads per block (because of the implementation).
Their configuration should, therefore, always be 2048/32.
The X compress and decompress kernels are more efficient when using a thread per block number of 64.
This is probably due to the size of 257 that must be loaded in shared memory (to perform the wavelet on the X axis).
The next bench will likely show that the best configuration is 257 threads per block and we will set it as the default.

#### Idea for next benchmark

After this interpretation, we have a good idea of what the best block configuration is.
We will set the configuration we have in mind for different grid sizes and compare these configurations to the previous "random" configurations.
We hope that the new configurations will be better or among the best configurations.

### Second benchmark

In the second benchmark, we we try to further tune the block configuration.
The first script is modified so that the thread per block number is fixed for all kernels.
This lets us more room to tune the number of blocks per grid.
We test the following block numbers (grid sizes): 65536 32768 16384 8192 4096 2048 1024 512 256 128 64
The default values for the thread per block number are: 32 for the YZ kernels, 512 for the step kernel, 512 for the shared compress/decompress kernels, 32 for the interface exchange kernels.
For the X wavelets kernels, we use the exact size needed to load the row into the shared memory (257 in our case).

We state the following exceptions:
- For A100 GPUs, the decompression (shared) kernel uses 128 threads per block instead of 512
- For A100 GPUs, the interface exchange kernels use 64 threads per block instead of 32
- For V100 GPUs, the decompression (shared) kernel uses 384 threads per block instead of 512
- For V100 GPUs, the decompression (shared) kernel uses 64 threads per block instead of 32

This benchmark has two goals:
- Refine the grid size (number of blocks). This is allowed because we set the thread per block number to a fixed value.
- Verify the performance of the new configurations compared to the previous ones (in particular, the wavelet X kernels).

We slightly modify the first script in order to incorporate these ideas.
The new benchmark was run with the script `script_used_for_benchmark_2.sh`.

#### Results

The results are shown in the files: block_config_GPU_WTYPE_2.csv, where GPU is a100, p100 or v100 and WTYPE is the wavelet type: shared (experiment 0) or global (experiment 1).

#### Interpretation

The first noticeable result is that the step kernel systematically performs better with a large number of blocks.
This is not surprising, since the step kernel is fine-grained and requires regular memory accesses, which is optimal for the GPU.
The GPU can, therefore, be used at its full potential with this kernel, which is reached with a 65536/512 configuration.

The interface exchange kernels seem to perform better with 256 blocks (512 for a100).
Lower and higher numbers of blocks lead to a decrease in performance.

The experiment tends to show that the wavelet X and YZ kernels can have any number of blocks between 2048 and 16384 (A100).
4096 seems to be the overall best configuration.

The wavelet X kernels are systematically slower than the configurations used in the previous experiment.
This suggests that the GPU hardware prefers to read 64 FP32 values 5 times, rather than 257 FP32 values once.
We take notice of this and stick to the suggested 64 threads per block configuration for the wavelet X kernels.

The shared wavelet kernels tend to perform better with a large number of blocks.
The overall best number of blocks tends to be 32768.
This is surprising, as each block processes and independent chunk of ths subgrid and there are 8*16*16=2048 chunks (we also refer to them as "blocks") in a subgrid.
There should, therefore, according to our understanding, be no improvements after 2048 blocks.
We, however, take notice of this and stick to the suggested 32768 number of blocks.

#### Conclusion (block configuration)

The two experiments show block configurations that perform well on large subgrids, such as 128x128x128.
The below table shows the configurations we aim to use for the next experiments.

| Kernel | GPU | Number of blocks | Number of threads per block |
| --- | --- | --- | --- |
| Step | A100 | 65536 | 512 |
| Step | P100 | 65536 | 512 |
| Step | V100 | 65536 | 512 |
| Interface exchange | A100 | 256 | 64 |
| Interface exchange | P100 | 256 | 32 |
| Interface exchange | V100 | 256 | 32 |
| Wavelet X | A100 | 4096 | 64 |
| Wavelet X | P100 | 4096 | 64 |
| Wavelet X | V100 | 4096 | 64 |
| Wavelet YZ | A100 | 4096 | 32 |
| Wavelet YZ | P100 | 4096 | 32 |
| Wavelet YZ | V100 | 4096 | 32 |
| Compression (shared) | A100 | 32768 | 512 |
| Compression (shared) | P100 | 32768 | 512 |
| Compression (shared) | V100 | 32768 | 512 |
| Decompression (shared) | A100 | 32768 | 128 |
| Decompression (shared) | P100 | 32768 | 512 |
| Decompression (shared) | V100 | 32768 | 384 |

#### Changes to the block configuration after other benchmarks

We slightly modified the configurations after some observations.
We realized that the best block number for the compression and decompression (shared) is:
```bash
	block_num_shared_compression_decompression=`expr $blocks_per_subgrid_x \* $blocks_per_subgrid_y \* $blocks_per_subgrid_z \* 27`
```
However, this might sometimes exceed the maximum number of blocks allowed by the GPU.
We hence, add the following code to the script:
```bash
	# Max grid size is 65536, so we'll balance the work optimally:
	# Divide by 2 until it is lower or equal than 65536
	# If not a multiple of two, set to 65536
	while [ "$block_num_shared_compression_decompression" -gt 65536 ] ; do
		if [ `expr $block_num_shared_compression_decompression % 2` -ne 0 ] ; then
			block_num_shared_compression_decompression=65536
			break
		fi
		block_num_shared_compression_decompression=`expr $block_num_shared_compression_decompression / 2`
	done
```

We also add the following tuning for the step kernel:
```bash
	thread_num_step=512
	block_num_step=65536

	if [ "$arch" = "p100" ] ; then
		thread_num_step=384
	fi
	# if use_mask, it takes additional registers
	if [ "$use_mask_object" -eq 1 ] ; then # TODO, this has not been tested on A100.
		if [ "$arch" = "v100" ] ; then
			thread_num_step=384
		else
			thread_num_step=256
		fi
		block_num_step=4096
	# if use_cylinder_object, it takes additional registers (but not as much as using the mask
	elif [ "$use_cylinder_object" -eq 1 ] ; then
		thread_num_step=384 # Fine_tuned ONLY for V100
		block_num_step=1024
	elif [ "$use_sphere_object" -eq 1 ] ; then
		thread_num_step=512
		if [ "$arch" = "a100" ] ; then
			block_num_step=4096 # Fine tuned for case 2000
		else
			block_num_step=2048 # Fine tuned for P100/V100 on case 2001 (by chance this is the best for both)
		fi
	fi
```
The part that is relevant to this study is only the `elif [ "$use_sphere_object" -eq 1 ]`, because the sphere case is more frequent in the literature.
The block number needs to be lower, because the sphere case has less regular bounce-back accesses (the block configuration benchmarks were performed with the cylinder case).

## Benchmark - realistic simulations

Experiment 0, 1, 2 and 3, 4, 5 are the same, except that 0, 1, 2 use the optimized compression kernels and 3, 4, 5 use the old compression kernels.
It's not worth elaborating too much on them.
Their purpose is to check that the mass is conserved whatever we do: with/without compression, with new/old compression kernels.
It also saves the data at a certain time step, which let's us verify that using subgrids does not change the result compared to working on a single grid.
It's just a proof that there are no significant bug in the code.

Experiments 6, 7, ..., 7+wavelet_tested_thresholds_length-1 are the threshold experiments.
wavelet_tested_thresholds is an array of possible values for the threshold.
Each experiment saves the data at a certain time step.
Experiment 6 serves as a reference for the other experiments.
Then, the result of the experiment i is compared to the result of the experiment 6.
In particular, metrics such as L2 loss, MSE, etc. can help us estimate the effective loss of the compression on the numerical scheme.
The goal of these experiments is to justify the choice of the threshold.
The main line is to say that even if we are not able to mathematically define the threshold, we can empirically see that there is a wide range of values that do not affect the numerical scheme and are still very compressive.

Helper, this command:
for i in ~/Sshfs/plafrim/d3q27_project/jobs/runs/2577381/experiment_*; do scp "$i"/build/data_exp_${i##*_}_t=*.csd .; done
can help retrieve the data from the cluster easily.
Similarly, this command:
for i in ~/Sshfs/plafrim/d3q27_project/jobs/runs/2577381/experiment_*; do scp "$i"/output.txt ./output_${i##*_}.txt; done
can help retrieve the output files, which can then be parsed to retrieve metrics such as the average compression ratio.

An other way with ssh: scp cflint@plafrim-ext:'/beegfs/cflint/rhea_benchmarks/2617509/experiment_*/data_exp_*_t=*.csd' .


### Threshold tests

Setting the threshold with the JPEG framework is always hard to justify, because it must offer a good trade-off between different metrics.
The threshold (normally) impacts:
- The compression ratio
- The simulation accuracy
- The compression/decompression time (due to the compression ratio)
Conceptually, a low threshold will lead to a high compression ratio, but also to a high simulation error, while a high threshold will lead to a low compression ratio, but also to a low simulation error.
Empirically, we see that there is often a range of values where the simulation error is negligible and the compression ratio is high.
We also often see a snowball effect as the threshold increases: the simulation adds up cumulative errors and the compression ratio decreases (because of new artifacts that must be compressed).

To justify this for our D3Q27 scheme, we run a series of simulations with different thresholds.
For each threshold, we run the simulation for a certain time and save the data.
We then plot two metrics: the loss (not chosen which type yet) and the compression ratio.

The python script `summary_loss_threshold_experiment.py` is used to summarize the important information.
Example usage:
`python3 summary_loss_threshold_experiment.py results_threshold_v3\ \(experiment\ 2617665\)/

## ......

### Generation of test cases with solid objects (obsolete)

***This section is obsolete.***

Test cases with a cylinder are easier to implement because we have a formula for the cylinder.
To have visually appealing results, we want to use solid objects with a complex shape than can correspond to a real need in the industry.
We can use the STL format to import the solid object.
The STL format is a format that describes the surface of a solid object and can be used to extract the voxelized version of the solid object.
We have two stl files: F-15_jet_plane.stl and xwing_Rescaled0.008_Rescaled1.5_rotated.stl.
We generate the voxelized version of the solid object with the utility stltovoxel:
<!-- stltovoxel spaceship/files/F-15_jet_plane.stl spaceship/F-15.png --resolution 200
stltovoxel spaceship/files/xwing_Rescaled0.008_Rescaled1.5_rotated.stl spaceship/xwings.png --resolution 140
stltovoxel spaceship/files/F-15_jet_plane.stl spaceship/F-15-a100.png --resolution 300
stltovoxel spaceship/files/xwing_Rescaled0.008_Rescaled1.5_rotated.stl spaceship/xwings-a100.png --resolution 200 -->
stltovoxel spaceship/files/F-15_jet_plane.stl spaceship/F-15.png --resolution 100
stltovoxel spaceship/files/xwing_Rescaled0.008_Rescaled1.5_rotated.stl spaceship/xwings.png --resolution 70
The resulting images are shown below correspond to slices at different z values.
The resolution is the number of z slices (minus 2) and is chosen carefully so that the object fit in the simulation space.

We removed the spaceship from the simulation for two reasons.
First, because using a solid object with a mask led to a significant decrease in performance, due to the difficulty of performing coalesced memory accesses.
Second, because it was not straightforward to implement a realistic low-Mach case that makes sense in practice.

### Execution and interpretation of the results

## Execution

The script `bench_runs.sh` is used to run the benchmarks on plafrim.
It generates multiple utility files that keep track of the used configuration, output, results (grid files), etc.
This allows us to run all the experiments once, and then analyze the results.

## Interpretation

Once the experiments are run, we can analyze the results.
For this we use different scripts, that are given in this repo.

# Threshold experiments

The goal is to give a glance at the impact of the different threshold values.
Experiments 100, 101, ... etc. are the threshold experiments.
Experiment 100 is the reference experiment (no compression).
The other experiments are compared to experiment 100.

The script `python3 summary_loss_threshold_experiment.py` is used to summarize the important information.
We use `python3 summary_loss_threshold_experiment.py job_number > summary_loss.txt` to save the summary in a file.
This script gives the MSE and RMSE for each experiment (compared to experiment 100).
It also prints of the average compression ratio and the used threshold.

The last used commands were:
`python3 summary_loss_threshold_experiment.py 'results_threshold_v3 (experiment 2617665)' > summary_loss.txt`
`python3 plot_threshold_experiments.py summary_loss.txt`

# Performance experiments

Same idea, we format the results nicely:
`python3 summary_performance_runs.py 2593091 > summary_performance_V100.txt`
Then use a python script for plotting:
`python3 plot_performance.py summary_performance_V100.txt`

## Performance up to t=0.2s

To have a glance at the performance that we will obtain, we run the performance experiments up to t=0.2s.
Runs 2609744 (A100), 2609924 (V100) and 2609925 (P100) are the results of the runs.
We generate the summary with the script `python3 summary_performance_runs.py 2609744 > summary_performance_A100_0.2s.txt`.
Then we use the scripts:
`python3 summary_performance_runs.py 2609925 > summary_performance_P100.txt`
`python3 summary_performance_runs.py 2609924 > summary_performance_V100.txt`
`python3 summary_performance_runs.py 2609744 > summary_performance_A100.txt`
Then plot with:
`python3 plot_performance.py summary_performance_P100.txt summary_performance_V100.txt summary_performance_A100.txt`
The result is stored in Figure_performance_t=0.2.png to keep track of this preliminary result.

## Performance up to t=5.0s

The results of this experiment are the directories 2610083, 2610084, and 2610085.
The results are mostly similar, but better because the simulation has more time to run (and is hence, a bit more realistic) and performs more time steps which reduces the variance.

It would be relevant to add more experiments with larger grid sizes, because it is possible and would demonstrate how larger the grid sizes can be.

## Last (hopefully) performance up to t=1.0s

The results of this experiment are the directories 2618700, 2618701, and 2618702.
We allow the simulation to run for a shorter time, because we observed that it does not change much between t=0.2s and t=5.0s.
The experiments with global compression run for 0.5s, because they are tremendously slow.

`python3 summary_performance_runs.py 2618702 > summary_performance_P100.txt`
`python3 summary_performance_runs.py 2618701 > summary_performance_V100.txt`
`python3 summary_performance_runs.py 2618700 > summary_performance_A100.txt`

Some data points are removed because of simimlarity with grid sizes in two experiments.
We take the best of the two points in this case, because it is expected that the programmer will choose the best configuration.
The removed points are documented directly within the files summary_performance_P100.txt, summary_performance_V100.txt, and summary_performance_A100.txt (in bash-like comments).

The data are still plot using `python3 plot_performance.py summary_performance_P100.txt summary_performance_V100.txt summary_performance_A100.txt`.

## Performance results for the PhD defense

`python3 plot_performance_phd_defense.py summary_performance_A100.txt`

## View of the performance with a table

The script `get_performance_table.py` outputs the latex code for a table that summarizes the performance of the different experiments.
It shows the percentage of time spent in the different kernels.

## Compression over time

The script `plot_compression_over_time.py` is used to plot the compression over time.
The input is an experiment (folder) number.
We generate the plots with run 2611703 which is the run that took 3 days.
Run 2615222 contains the results two experiments for smaller grid sizes.
Hence, the plot can be generated with the command:
`python3 plot_compression_over_time.py 2635398/experiment_10000/ 2635369/experiment_10001/ 2635369/experiment_10002/ 2647459/experiment_12005/`

Update, the run ids are different now.
python3 plot_compression_over_time.py 2635398/experiment_10000/ 2635369/experiment_10001/ 2635369/experiment_10002/

## Visual results

We use a custom python script (based on mayavi) to visualize the results.
The only purpose of this is to ensure that the numerical scheme has been implemented correctly and that we see the emergence of the expected physical phenomena.
The used commands were:
`python3 plot_3d.py results_10002_plafrim_v2/data_exp_10002_t\=1000.0097_all.csd -s 'sphere' -c 14 4 0 -f 0 4 0 -a 30`
`python3 plot_3d.py results_10001_plafrim_v2/data_exp_10001_t\=1000.0060_part.csd -s 'sphere' -M 396 1632 408 -c 14 4 0 -f 0 4 0 -a 30`

`python3 plot_3d.py data_exp_11003_t\=600.0171_all.csd -s 'sphere' -c 4 -6 -4 -f 0 2 0 -u 1 0 -1 -a 30` gives a good angle for reynolds=230

Update: new simulations have been run to have better comparisons with or without compression.
The new commands are:
`python3 plot_3d.py data_exp_11005_t\=600.0126_all.csd -s 'sphere' -c -10 4 0 -f  0 4 0 -u 0 0 -1 -a 30` (small, no compression)
`python3 plot_3d.py data_exp_12005_t\=600.0126_all.csd -s 'sphere' -c -10 4 0 -f  0 4 0 -u 0 0 -1 -a 30` (small, with compression)
`python3 plot_3d.py results_large_plafrim_final/data_exp_10000_t=1000.0048_part.csd -s 'sphere' -M 693 2856 714 -c -4.8 4.4 -1.4 -f -0.5 2 -0.15 -u 0 0 -1 -a 30` (large grid with compression)

The first two images are resized like so:
`convert Figure_D3Q27_t600_small_no_compression.png -crop 1512x945+100+000 +repage -resize 1600x1000 output.png`
by chance, the third image (which is a different camera angle) can be resized with the exact same parameters:
`convert Figure_D3Q27_t1000_large.png -crop 1512x945+100+000 +repage -resize 1600x1000 output.png`



### Restuts with StarPU

Some results with StarPU are also provided to provide a reference point.

Basically, three plots, each basing on an automatically generated summary file.

Generage the summary files:
`python3 summary_schedulers.py 2800210_schedulers 2800210_schedulers_2/ > summary_schedulers.txt`
`python3 summary_scaling.py 2798284_weak_scaling/ > summary_weak_scaling.txt`
`python3 summary_scaling.py 2797170_strong_scaling/ > summary_strong_scaling.txt`

Then plot the results:
`python3 plot_schedulers.py summary_schedulers.txt`
`python3 plot_scaling.py summary_weak_scaling.txt 'Average execution time of a D3Q27 simulation (weak scaling)'`
`python3 plot_scaling.py summary_strong_scaling.txt 'Average execution time of a D3Q27 simulation (strong scaling)'`