#!/bin/sh

# function execute_command {
# 	OUTPUT_FILE=$(mktemp)
# 	use_CPU=$use_CPU RHEA_BENCHMARK_PRECISION=$RHEA_BENCHMARK_PRECISION RHEA_BENCHMARK_SUBGRID_SIZE=$RHEA_BENCHMARK_SUBGRID_SIZE RHEA_BENCHMARK_GRID_SIZE=$RHEA_BENCHMARK_GRID_SIZE RHEA_BENCHMARK_OVERLAP=$RHEA_BENCHMARK_OVERLAP RHEA_BENCHMARK_DIMENSIONS=2 RHEA_BENCHMARK_MAX_ITE=$RHEA_BENCHMARK_MAX_ITE RHEA_CUDA_MAX_BLOCKS=128 RHEA_CUDA_MAX_THREADS_PER_BLOCK=65536 RHEA_COPY_SLICE_CUDA_MAX_BLOCKS=64 RHEA_COPY_SLICE_CUDA_MAX_THREADS_PER_BLOCK=8192 RHEA_BENCHMARK_SAVE_FILES=0 STARPU_SCHED=dmda STARPU_NWORKER_PER_CUDA=$nworkerspergpu STARPU_NCUDA=$STARPU_NCUDA STARPU_NCPU=$STARPU_NCPU bash -c "numactl -i all $benchmarked_command" >$OUTPUT_FILE 2>&1
# 	command_output="$(< "$OUTPUT_FILE")"
# 	rm -f "$OUTPUT_FILE"
# }

job_id=$SLURM_JOB_ID
if [ -z "$job_id" ] ; then
	echo "Warning: SLURM_JOB_ID is not set, using 42 instead"
	job_id=42
fi

root_directory=`pwd`
runs_directory="$root_directory/jobs/runs"
job_output_directory="$runs_directory/$job_id"
d3q27_source_directory="$root_directory/wavelet_cuda"

# Load modules
source "$d3q27_source_directory/modules.sh"

if [ -d "$job_output_directory" ] ; then
	echo "folder $job_output_directory already exists, aborting"
	exit
fi
mkdir -p "$job_output_directory"

job_log_file="$job_output_directory/log"
job_config_file="$job_output_directory/config"
touch "$job_log_file"
touch "$job_config_file"

v100_num=`nvidia-smi -L | grep V100 | wc -l`
p100_num=`nvidia-smi -L | grep P100 | wc -l`
a100_num=`nvidia-smi -L | grep A100 | wc -l`

# max_gpu_type_num=`printf "$v100_num\n$p100_num\n$k40m_num\n" | awk 'n < $0 {n=$0}END{print n}'`
max_gpu_type_num=`printf "$v100_num\n$p100_num\n$a100_num\n" | awk 'n < $0 {n=$0}END{print n}'`

if [ `expr "$v100_num" + "$p100_num" + "$a100_num"` -eq 0 ] ; then
	echo "gpus are not of same type, aborting" >> $job_log_file
	exit
fi

arch='unknown'
vram_size_gb=-1

if [ "$v100_num" -gt 0 ] ; then
	arch='v100'
	maxgpu=2
	target_architecture="V100"
	vram_size_gb=15
	vram_size_b="((long)(((size_t)1)<<30)*(long)$vram_size_gb)"
fi
if [ "$p100_num" -gt 0 ] ; then
	arch='p100'
	maxgpu=2
	target_architecture="P100"
	vram_size_gb=15
	vram_size_b="((long)(((size_t)1)<<30)*(long)$vram_size_gb)"
fi
if [ "$a100_num" -gt 0 ] ; then
	arch='a100'
	maxgpu=2
	target_architecture="A100"
	vram_size_gb=38
	vram_size_b="((long)(((size_t)1)<<30)*(long)$vram_size_gb)"
fi

cd "$job_output_directory"

echo 'v100_num='$v100_num >> $job_config_file
echo 'p100_num='$p100_num >> $job_config_file
echo 'a100_num='$a100_num >> $job_config_file
echo 'arch='$arch >> $job_config_file
echo 'hostname='$HOSTNAME >> $job_config_file
echo 'job_id='$job_id >> $job_config_file
echo 'node_name='$SLURMD_NODENAME >> $job_config_file
echo 'lscpu :' >> $job_config_file
lscpu >> $job_config_file
echo '#########################################' >> $job_config_file
echo 'nvidia-smi :' >> $job_config_file
nvidia-smi >> $job_config_file

for experiment_configuration in 0 1
do
	experiment_output_file="$job_output_directory/experiment_$experiment_configuration/output"
	experiment_csv_file="$job_output_directory/experiment_$experiment_configuration/block_tests.csv"
	experiment_log_file="$job_output_directory/experiment_$experiment_configuration/log"
	build_directory="$job_output_directory/experiment_$experiment_configuration/build"

	mkdir -p "$job_output_directory/experiment_$experiment_configuration"

	touch "$experiment_output_file"
	touch "$experiment_csv_file"
	touch "$experiment_log_file"

	# if [ "$experiment_configuration" -eq 0 ] ; then
	# 	echo 'config;assign_subgrid;forwardDWTGridLocallyCSR;inverseDWTGridLocallyCSR;save_interfaces<bool=0>;save_interfaces<bool=1>;step' >> $experiment_csv_file
	# elif [ "$experiment_configuration" -eq 1 ] ; then
	# 	echo 'config;assign_subgrid;cusparseDenseToCsr_kernel2;cusparseParseDenseByRows_kernel;fast_wavelet_x_kernel (decompress);fast_wavelet_x_kernel (compress);...' >> $experiment_csv_file
	# fi
	echo 'config;Step;Interface to subgrid;Subgrid to interface;Compress shared;Decompress shared;Compress global X;Decompress global X;Compress global YZ;Decompress global YZ;cusparseD2CSR;cusparseCSR2D' >> $experiment_csv_file

	for block_num in 65536 32768 16384 8192 4096 2048 1024 512 256 128 64
	do
		for block_dim in 512
		do
			mkdir -p "$build_directory" && cd "$build_directory"

			execution_output_file="$job_output_directory/experiment_$experiment_configuration/output_block_${block_num}_dim_${block_dim}.txt"

			touch "$execution_output_file"

			# default values for the experiments
			cmake_build_type="Release"
			compute_mass=0
			paranoid_asserts=0

			subgrid_number_x=2
			subgrid_number_y=8
			subgrid_number_z=2

			use_mask_object=0

			# Set up the cmake variables for this experiment
			if [ "$experiment_configuration" -eq 0 ] ; then
				subgrid_csr_compression=0

				blocks_per_subgrid_x=8
				blocks_per_subgrid_y=16
				blocks_per_subgrid_z=16

				block_size_x=33
				block_size_y=17
				block_size_z=17
			elif [ "$experiment_configuration" -eq 1 ] ; then
				subgrid_csr_compression=1
			
				# if subgrid_csr_compression, blocks_per_subgrid_x represent the size of the subgrid, not the number of blocks
				blocks_per_subgrid_x=257
				blocks_per_subgrid_y=257
				blocks_per_subgrid_z=257

				block_size_x=1
				block_size_y=1
				block_size_z=1
			fi

			# owned size = block_size * blocks_per_subgrid
			subgrid_owned_x=`expr $block_size_x \* $blocks_per_subgrid_x`
			subgrid_owned_y=`expr $block_size_y \* $blocks_per_subgrid_y`
			subgrid_owned_z=`expr $block_size_z \* $blocks_per_subgrid_z`

			block_num_decompression_shared=$block_num
			thread_num_decompression_shared=512
			if [ "$arch" = "a100" ] ; then
				thread_num_decompression_shared=128
			fi
			if [ "$arch" = "v100" ] ; then
				block_num_decompression_shared=256
				thread_num_decompression_shared=384
			fi

			thread_num_interface_exchange=32
			if [ "$arch" = "a100" ] ; then
				thread_num_interface_exchange=64
			fi

			# # for shared compression/decompression, the best block number is presumably blocks_per_subgrid_x*blocks_per_subgrid_y*blocks_per_subgrid_z
			# block_num_shared=`expr $blocks_per_subgrid_x \* $blocks_per_subgrid_y \* $blocks_per_subgrid_z`

			# purge cmake cache
			rm CMakeCache.txt
			rm -r CMakeFiles

			cd "$build_directory"

			cmake_command="cmake -DTARGET_ARCH=$target_architecture -DCOMPUTE_MASS=$compute_mass -DPARANOID_ASSERTS=$paranoid_asserts -DCMAKE_BUILD_TYPE=$cmake_build_type\
				-DBLOCKS_PER_SUBGRID_X=$blocks_per_subgrid_x -DBLOCKS_PER_SUBGRID_Y=$blocks_per_subgrid_y -DBLOCKS_PER_SUBGRID_Z=$blocks_per_subgrid_z\
				-DSUBGRID_NUMBER_X=$subgrid_number_x -DSUBGRID_NUMBER_Y=$subgrid_number_y -DSUBGRID_NUMBER_Z=$subgrid_number_z\
				-DUSE_MASK_OBJECT=$use_mask_object\
				-DBLOCK_DIM_WAVELET_X=$subgrid_owned_x -DBLOCK_NUM_WAVELET_X=$block_num\
				-DBLOCK_DIM_WAVELET_Y=32 -DBLOCK_NUM_WAVELET_Y=$block_num\
				-DBLOCK_DIM_STEP=512 -DBLOCK_NUM_STEP=$block_num\
				-DBLOCK_DIM_INVERSE_DWT_GRID_LOCALLY_CSR=$thread_num_decompression_shared -DBLOCK_NUM_INVERSE_DWT_GRID_LOCALLY_CSR=$block_num_decompression_shared\
				-DBLOCK_DIM_FORWARD_DWT_GRID_LOCALLY_CSR=512 -DBLOCK_NUM_FORWARD_DWT_GRID_LOCALLY_CSR=$block_num\
				-DBLOCK_DIM_NO_COMPRESSION_FAKE_KERNEL=$block_dim -DBLOCK_NUM_NO_COMPRESSION_FAKE_KERNEL=$block_num\
				-DBLOCK_DIM_REMOVE_OVERLAP=$block_dim -DBLOCK_NUM_REMOVE_OVERLAP=$block_num\
				-DBLOCK_DIM_SUBGRID_TO_INTERFACE=$thread_num_interface_exchange -DBLOCK_NUM_SUBGRID_TO_INTERFACE=$block_num\
				-DBLOCK_DIM_INTERFACE_TO_SUBGRID=$thread_num_interface_exchange -DBLOCK_NUM_INTERFACE_TO_SUBGRID=$block_num\
				-DGPU_MEM_ALLOCATE="$vram_size_b"\
				-DSUBGRID_LEVEL_CSR_COMPRESSION=$subgrid_csr_compression\
				$d3q27_source_directory"

			echo "Used cmake command:" >> $experiment_log_file
			echo "$cmake_command" >> $experiment_log_file

			# Generate Makefile with the cmake command
			$cmake_command

			# Build
			make

			# Put the block num/dim in the config file
			echo -n "${block_num}_$block_dim" >> $experiment_csv_file

			##### This old code based the benchmark on the nvprof output, which is deprectated
			# nvprof_command="nvprof"
			# # On A100, the command is "nsys nvprof"
			# if [ "$arch" = "a100" ] ; then
			# 	nvprof_command="nsys nvprof"
			# fi
			# # Execute the benchmark
			# output_command=`$nvprof_command ./D3Q27 -t 0.05 2>&1`
			# echo "$output_command" >> $execution_output_file
			# csv_output=`cat "$execution_output_file" | sed -n '/GPU activities:/,/API calls:/p' | python3 "$d3q27_source_directory/parse_output_nvprof.py" | awk -F';' '{printf ";%s", $2}'`
			# # csv_output = ";0.26788;5.1191;1.4101;0.42135;0.33654;0.6629400000000001" for example
			# echo "$csv_output" >> $experiment_csv_file

			##### This code bases the results on the timers
			# Execute the benchmark
			output_command=`./D3Q27 -t 0.05 2>&1`
			echo "$output_command" >> $execution_output_file
			csv_output=`cat "$execution_output_file" | awk -F'avg): ' '/avg):/{gsub(/ ms.*/, "", $2); printf (NR>1?";":"") "%s", $2}'`
			# csv_output = ";0.26788;5.1191;1.4101;0.42135;0.33654;0.6629400000000001" for example
			echo "$csv_output" >> $experiment_csv_file


			# cd back to the root directory
			cd "$root_directory"
		done
	done
done

# echo "with CPU;precision;grid size;subgrid size;overlap;CUDA number;CPU number" >> $job_csv_file

# for use_CPU in 0 1
# do
# 	for RHEA_BENCHMARK_PRECISION in 0 1
# 	do
# 		for RHEA_BENCHMARK_GRID_SIZE in 2048 4096 8192
# 		do
# 			for RHEA_BENCHMARK_SUBGRID_SIZE in $RHEA_BENCHMARK_GRID_SIZE `expr $RHEA_BENCHMARK_GRID_SIZE / 2` `expr $RHEA_BENCHMARK_GRID_SIZE / 4` `expr $RHEA_BENCHMARK_GRID_SIZE / 8`
# 			do
# 				if [ "$RHEA_BENCHMARK_SUBGRID_SIZE" -gt 4096 ]; then  
# 					continue
# 				fi
# 				for RHEA_BENCHMARK_OVERLAP in 1 2 4 8
# 				do
# 					# for each possible gpu num
# 					for (( STARPU_NCUDA=1 ; STARPU_NCUDA <= maxgpu ; STARPU_NCUDA++ )) ; do
# 						# N executions for each case
# 						for (( n=0 ; n <= 1 ; n++ )) ; do
# 							# The first execution only warms up the perfmodels
# 							RHEA_BENCHMARK_DIMENSIONS=2
# 							RHEA_BENCHMARK_MAX_ITE=`expr 4294967296 / $RHEA_BENCHMARK_GRID_SIZE / $RHEA_BENCHMARK_GRID_SIZE`
# 							RHEA_CUDA_MAX_BLOCKS=128
# 							RHEA_CUDA_MAX_THREADS_PER_BLOCK=65536
# 							RHEA_COPY_SLICE_CUDA_MAX_BLOCKS=64
# 							RHEA_COPY_SLICE_CUDA_MAX_THREADS_PER_BLOCK=8192
# 							RHEA_BENCHMARK_SAVE_FILES=0
# 							STARPU_SCHED=dmda
# 							STARPU_NWORKER_PER_CUDA=$nworkerspergpu

# 							STARPU_NCPU=`expr $ncpus - $STARPU_NCUDA \* $STARPU_NWORKER_PER_CUDA`

# 							if [ $use_CPU=1 ]; then  
# 								benchmarked_command="$build_directory/tests/benchmark_CPU_CUDA"
# 							else
# 								benchmarked_command="$build_directory/tests/benchmark_only_CUDA"
# 							fi

# 							if [ "$n" -eq 0 ] ; then
# 								#$benchmarked_command >> $job_log_file 2> $job_log_file # was just for testing

# 								# launch an unbencmarked execution
# 								execute_command

# 								printf "$use_CPU;$RHEA_BENCHMARK_PRECISION;$RHEA_BENCHMARK_SUBGRID_SIZE;$RHEA_BENCHMARK_GRID_SIZE;$RHEA_BENCHMARK_OVERLAP;$STARPU_NCUDA;$STARPU_NCPU;" >> $job_csv_file
# 							else
# 								execute_command
# 								echo "$command_output" | awk '/time=/{ printf $0 }' | sed "s/.*time=\(.*\)s.*/\1/g" >> $job_csv_file
# 								printf ';' >> $job_csv_file
# 							fi
# 						done
# 						echo "" >> $job_csv_file
# 					done
# 				done
# 			done
# 		done
# 	done
# done

