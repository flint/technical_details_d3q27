import sys
import matplotlib.pyplot as plt



# Read a file from args

# The file must look like this:
# Experiment 101:
# Threshold: 0.0
# Average compression ratio: 48.45915898507463
# Average error: 2.1106388494887242e-14
# Experiment 102:
# Threshold: 0.0
# Average compression ratio: 68.20426762686567
# Average error: 2.3333139285762805e-14
# Experiment 103:
# Threshold: 0.0
# Average compression ratio: 75.54045674626867
# Average error: 3.141971005582684e-14
# Experiment 104:
# Threshold: 0.0
# ...

# This script will plot the average compression ratio vs the error for each experiment
# The X axis will be the error
# The Y axis will be the average compression ratio

# Read the file
if len(sys.argv) < 2:
    print('Usage: python3 plot_threshold_experiments.py <file>')
    sys.exit(1)

filename = sys.argv[1]

# Array of results, will look like this: [{'threshold': 0.0, 'average_compression_ratio': 48.45915898507463, 'error': 2.1106388494887242e-14}, ...]
results = []

# Correct the threshold values because of a bad formatting in the original code
# corrected_thresholds = wavelet_tested_thresholds=[0.00000001, 0.000000015, 0.00000002, 0.00000003, 0.00000004, 0.00000006, 0.00000008, 0.0000001, 0.00000015, 0.0000002, 0.0000003, 0.0000004, 0.0000006, 0.0000008, 0.000001, 0.0000015, 0.000002, 0.000003, 0.000004, 0.000006, 0.000008, 0.00001, 0.000015, 0.00002, 0.00003, 0.00004, 0.00006, 0.00008, 0.0001]

# Read the file
with open(filename, 'r') as f:
    lines = f.readlines()

    # Iterate over the lines
    for line in lines:
        # If the line starts with "Experiment ", then we found the line we want
        if line.startswith('Experiment '):
            # Split the line by spaces
            line_split = line.split(' ')
            # The last element of the split line is the experiment number
            experiment_number = int(line_split[-1][:-2])

            # The next two lines are the threshold and the average compression ratio
            # threshold = corrected_thresholds[experiment_number - 101]
            threshold = float(lines[lines.index(line) + 1].split(' ')[-1])
            average_compression_ratio = float(lines[lines.index(line) + 2].split(' ')[-1])

            # The next line is the average error
            error = float(lines[lines.index(line) + 3].split(' ')[-1])

            # Add the result to the list
            results.append({'experiment_number': experiment_number, 'threshold': threshold, 'average_compression_ratio': average_compression_ratio, 'error': error})

print(results)

# # Remove the first result, which is the one with the threshold 0.0
# results = results[1:]

# Plot the results with pyplot
# Colorblind friendly colors
# CB_color_cycle = ['#377eb8', '#ff7f00', '#4daf4a',
#                   '#f781bf', '#a65628', '#984ea3',
#                   '#999999', '#e41a1c', '#dede00']

# Increase the default size of the figure
plt.rcParams["figure.figsize"] = (12, 8)

# Create the figure
fig, ax1 = plt.subplots()

ax1.set_xlabel('Error (NMSE)', fontsize=24)
ax1.set_ylabel('Compression ratio', fontsize=24)

ax1.set_xlim(2e-7, 1e-4)
ax1.set_ylim(18, 1e2)

# Increase the size of the ticks
ax1.tick_params(axis='both', which='major', labelsize=16)

# Plot the datapoints (Error, compression ratio)
for result in results:
    ax1.scatter(result['error'], result['average_compression_ratio'], color='blue')

# Use log scales
ax1.set_yscale('log')
ax1.set_xscale('log')

plt.show()
