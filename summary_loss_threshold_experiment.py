import sys
import os
import gzip
import numpy as np
import time

# We have a script diff_grid.
# If we call it like this:
# python3 diff_grid.py 2591743/saves/experiment_100/data_exp_100_t\=1.0043_all.csd 2591743/saves/experiment_101/data_exp_101_t\=1.0043_all.csd 

# We get an output like this:
# Experiment 101:
# Threshold: 0.0
# Average compression ratio: 48.45915898507463
# Average error: 2.1106388494887242e-14
# Experiment 102:
# Threshold: 0.0
# Average compression ratio: 68.20426762686567
# Average error: 2.3333139285762805e-14
# Experiment 103:
# Threshold: 0.0
# Average compression ratio: 75.54045674626867
# Average error: 3.141971005582684e-14
# Experiment 104:
# Threshold: 0.0
# ...

# What we are interested only in the error (and the threshold)

# This python script (summary_loss_threshold_experiment.py) will run diff_grid.py for all the relevant files in a directory, and will print a nice summary of the results.

# For example, if we run it like this:
# python3 summary_loss_threshold_experiment.py 2591743
# We will call the commands:
# python3 diff_grid.py 2591743/saves/experiment_100/data_exp_100_t\=1.0043_all.csd 2591743/saves/experiment_101/data_exp_101_t\=1.0043_all.csd 
# python3 diff_grid.py 2591743/saves/experiment_100/data_exp_100_t\=1.0043_all.csd 2591743/saves/experiment_102/data_exp_102_t\=1.0043_all.csd 
# ...
# python3 diff_grid.py 2591743/saves/experiment_100/data_exp_100_t\=1.0043_all.csd 2591743/saves/experiment_199/data_exp_199_t\=1.0043_all.csd

# And parse their results.

# The output will be something like this:
#
# Experiment 101:
# Threshold: 0.0
# Average error: 2.1106388494887242e-14
# Experiment 102:
# Threshold: 0.0
# Average error: 2.3333139285762805e-14
# Experiment 103:
# ...



# We assume filename1 is the reference density and filename2 is the tested density
def extract_difference_files(filename1, filename2, target_c=[0]):
    with gzip.open(filename1, 'rt') as file1, gzip.open(filename2, 'rt') as file2:
        # Global size of the grid
        X, Y, Z, C = map(int, file1.readline().split(','))
        X2, Y2, Z2, C2 = map(int, file2.readline().split(','))

        dx = np.float64(4)/np.float64(X)
        dy = dx
        dz = dx

        # dx ~= dy ~= dz ; dx, dy, dz are close
        assert np.isclose(dx, dy) and np.isclose(dy, dz), 'Grid dimensions are not close'

        # Interval of the grid, which matters if we saved a part of the grid
        X_min, Y_min, Z_min, X_max, Y_max, Z_max = map(int, file1.readline().split(','))
        X_min2, Y_min2, Z_min2, X_max2, Y_max2, Z_max2 = map(int, file2.readline().split(','))

        assert X == X2 and Y == Y2 and Z == Z2 and C == C2, 'Grid dimensions do not match'
        assert X_min == X_min2 and Y_min == Y_min2 and Z_min == Z_min2, 'Grid min dimensions do not match'
        assert X_max == X_max2 and Y_max == Y_max2 and Z_max == Z_max2, 'Grid max dimensions do not match'

        X = X_max - X_min + 1
        Y = Y_max - Y_min + 1
        Z = Z_max - Z_min + 1

        percentage_of_c_to_process = len(target_c) / C

        # declare sum and current_sum as double precision to increase the precision of the reduction
        sum_squared = np.float64(0.0)
        sum_squared_reference = np.float64(0.0)
        for c in range(C):
            if c not in target_c:
                for z in range(Z):
                    for y in range(Y):
                        file1.readline()
                        file2.readline()
                continue
            for z in range(Z):
                for y in range(Y):
                    line1 = file1.readline().strip().split(' ')
                    line2 = file2.readline().strip().split(' ')
                    
                    # If the lines (as strings) are not equal, print them
                    # if line1 != line2:
                    #     print(f'line1: {line1}')
                    #     print(f'line2: {line2}')
                    # else:
                    #     print(f'line1 == line2: {line1} == {line2}')

                    # Ignore empty lines
                    while len(line1) < X:
                        line1 = file1.readline().strip().split(' ')
                    while len(line2) < X:
                        line2 = file2.readline().strip().split(' ')

                    assert len(line1) == X, 'Invalid line length: {}'.format(len(line1))
                    assert len(line2) == X, 'Invalid line length: {}'.format(len(line2))

                    # double precision to increase the precision of the reduction
                    fp_array1 = np.array(line1, dtype=np.float64)
                    fp_array2 = np.array(line2, dtype=np.float64)

                    # Add (sum1 - sum2)^2 / sum1 to the error
                    sum_squared += np.sum(dx*dy*dz*np.square(fp_array1 - fp_array2))
                    sum_squared_reference += np.sum(dx*dy*dz*np.square(fp_array1))

                    # for i in range(len(fp_array1)):
                    #     if np.abs(fp_array1[i]-fp_array2[i]) > 0.01:
                    #         print(f'({i}, {y}, {z}, {c}): {fp_array1[i]} != {fp_array2[i]}')

                    # print(f'sum arr1: {np.sum(fp_array1)}')
                    # print(f'sum arr2: {np.sum(fp_array2)}')

                    # # if not in range [0.9, 1.1], print the values
                    # if fp_array1.min() < 0.9 or fp_array1.max() > 1.1:
                    #     print(f'There are values out of range at ({c}, {z}, {y}, X)')
                    # if fp_array2.min() < 0.9 or fp_array2.max() > 1.1:
                    #     print(f'There are values out of range at ({c}, {z}, {y}, X)')

        return np.sqrt(sum_squared) / np.sqrt(sum_squared_reference)





# Parse from stdarg the filename
if len(sys.argv) < 2:
    print('Usage: python3 summary_loss_threshold_experiment.py <directory>')
    sys.exit(1)

directory = sys.argv[1]

for i in range(102, 200):
    filename1 = f'{directory}/data_exp_101_t=1.0043_all.csd'
    filename2 = f'{directory}/data_exp_{i}_t=1.0043_all.csd'
    output_of_benchmark = f'{directory}/output_{i}.txt'

    # Verify that everything exists
    if not os.path.exists(f'{directory}') or not os.path.exists(filename1) or not os.path.exists(filename2) or not os.path.exists(output_of_benchmark):
        continue

    # Read the output file to get the threshold used for the experiment and the average compression ratio
    with open(output_of_benchmark, 'r') as f:
        lines = f.readlines()

        # The line of the output that gives the threshold looks like "ceil_value = 0.000000"
        # We want to extract the 0.000000

        # The lines of the output that give the compression ratio of an iteration look like this:
        # "end of iteration 0(t=0.0173): compression rate (real)=/184.467175(5797785600/31429904)"
        # We want to sum the compression ratios of all the time steps (184.467175, for this one)

        threshold=-1
        compression_ratio_sum = 0.0
        num_iterations = 0
        
        # Iterate over the lines:
        for line in lines:
            # If the line starts with "ceil_value = ", then we found the line we want
            if line.startswith('ceil_value = '):
                # Split the line by spaces
                line_split = line.split(' ')
                # The last element of the split line is the threshold
                threshold = float(line_split[-1])
            if line.startswith('end of iteration ') and 'compression rate' in line:
                # Split the line by spaces
                line_split = line.split('/')
                line_split_2 = line_split[1].split('(')
                compression_ratio_sum += float(line_split_2[0])

                num_iterations += 1


    print(f'Experiment {i}:')

    print(f'Threshold: {threshold}')
    print(f'Average compression ratio: {compression_ratio_sum / num_iterations}')

    # Parse the file and get grid data
    error = extract_difference_files(filename1, filename2, target_c=[0])
    print('Average error:', error)

