import sys
import os
import gzip
import numpy as np
import time

# We get an output like this:

# This python script (summary_schedulers.py) will look at the folder given as argument, and will print a nice summary of the results.

# For example, if we run it like this:
# python3 summary_schedulers.py 2797170_4_nodes_smaller
# It will analyze the following files:
# 2797170_4_nodes_smaller/experiment_1000000/output.txt
# 2797170_4_nodes_smaller/experiment_1000001/output.txt
# ...
# 2797170_4_nodes_smaller/experiment_1999999/output.txt (if exists)

# And then parse their results.

# The output will be something like this:
#
# Experiment 10000:
# Execution time: 3.912142
# Idle time (avg): 1.5234
# GB processed per second: 4.4170175827
# Experiment 10001:
# ...


# Parse from stdarg the filename
if len(sys.argv) < 2:
    print('Usage: python3 summary_schedulers.py <directory1> <directory2> ...')
    sys.exit(1)

# directory = sys.argv[1]
directories = sys.argv[1:]

for directory in directories:
    print(f'Analyzing directory {directory}')
    for i in range(1000000, 10000000):
        filename = f'{directory}/experiment_{i}/output.txt'

        # avoid overloading the process, as we know experiments should be of the form 10000xx
        if (i % 1000000) >= 1000:
            continue

        # Verify that the file exists
        if not os.path.exists(filename):
            continue

        # Read the output file to get the threshold used for the experiment and the average compression ratio
        with open(filename, 'r') as f:
            lines = f.readlines()

            # The line of the output that gives the Total grid logical size looks like this: "Total grid logical size = 108.000000 MB"
            # The one that gives the number of iterations looks like this: "Performed 160 iterations"
            # The one that gives the total time looks like this: "[rank 2] Global time: 2.309206 s"
            # There are multiple global times (one per rank), we use the largest one:
            # total_time = max(global_time_rank_0, global_time_rank_1, ...)
            # The GB processed per second is computed like this: (grid_size/1024)*num_iterations/total_time*2 (the *2 is because we have two grids)

            grid_size = -1
            num_iterations = -1
            total_time = -1
            rank_times=np.zeros(16)
            
            # Iterate over the lines:
            for line in lines:
                if line.startswith('Total grid logical size = '):
                    # Split the line by spaces
                    line_split = line.split(' ')
                    # The last element of the split line is the grid size
                    grid_size = float(line_split[-2])
                if line.startswith('Performed ') and 'iterations' in line:
                    # Split the line by spaces
                    line_split = line.split(' ')
                    # The second element of the split line is the number of iterations
                    num_iterations = int(line_split[1])
                if line.startswith('[rank ') and 'Global time: ' in line:
                    rank= int(line.split(' ')[1].replace(']', ''))
                    rank_time = float(line.split(' ')[4])
                    rank_times[rank]=rank_time


        total_time = max(rank_times)
        gb_processed_per_second = (grid_size/1024)*num_iterations/total_time*2


        print(f'Experiment {i}:')

        if i >= 1000000 and i <= 1999999:
            print(f'Scheduler: Heteroprio')
        elif i >= 2000000 and i <= 2999999:
            print(f'Scheduler: dmda')
        elif i >= 3000000 and i <= 3999999:
            print(f'Scheduler: dmdas')
        elif i >= 4000000 and i <= 4999999:
            print(f'Scheduler: lws')
        else:
            print(f'Scheduler: Unknown')
        
        print(f'Total grid logical size: {grid_size}')
        print(f'Number of iterations: {num_iterations}')
        print(f'Total time: {total_time}')
        print(f'GB processed per second: {gb_processed_per_second}')

