import sys
import os
import gzip
import numpy as np
import time

# We get an output like this:

# This python script (summary_performance_runs.py) will look at the folder given as argument, and will print a nice summary of the results.
# The goal is to plot different data points on a graph, where the Y axis is the GB processed per second, and the X axis is the total size of the logical subgrid

# For example, if we run it like this:
# python3 summary_loss_threshold_experiment.py 2591743
# It will analyize the following files:
# 2591743/experiment_1000/output.txt
# 2591743/experiment_1001/output.txt
# ...
# 2591743/experiment_2000/output.txt
# 2591743/experiment_2001/output.txt
# ...
# 2591743/experiment_3000/output.txt
# 2591743/experiment_3001/output.txt
# ...
# 2591743/experiment_3999/output.txt (if exists)

# And then parse their results.

# The output will be something like this:
#
# Experiment 1000:
# Compression type: None
# Total grid logical size: 108.0
# Number of iterations: 160
# Total time: 3.912142
# GB processed per second: 4.4170175827
# Subgrid number x: 4
# Experiment 1001:
# ...


# Parse from stdarg the filename
if len(sys.argv) < 2:
    print('Usage: python3 summary_loss_threshold_experiment.py <directory>')
    sys.exit(1)

directory = sys.argv[1]

for i in range(1000,4999):
    filename = f'{directory}/experiment_{i}/output.txt'

    # Verify that the file exists
    if not os.path.exists(filename):
        continue

    # Read the output file to get the threshold used for the experiment and the average compression ratio
    with open(filename, 'r') as f:
        lines = f.readlines()

        # If between 1000 and 1999 included, the compression type is "None"
        # If between 2000 and 2999 included, the compression type is "Shared"
        # If between 3000 and 3999 included, the compression type is "Global"
        # If between 4000 and 4999 included, the compression type is "None, but subgrids"

        # The line of the output that gives the Total grid logical size looks like this: "Total grid logical size = 108.000000 MB"
        # The one that gives the number of iterations looks like this: "Performed 160 iterations"
        # The one that gives the total time looks like this: "Total time: 3.912142 s"
        # The GB processed per second is computed like this: (grid_size/1024)*num_iterations/total_time*2 (the *2 is because we have two grids)
        # subgrid_num_x: "Subgrid number per dimension = 4 16 4" means 4x16x4, so 4 is the subgrid_num_x

        grid_size = -1
        num_iterations = -1
        total_time = -1
        subgrid_num_x = -1
        
        # Iterate over the lines:
        for line in lines:
            if line.startswith('Total grid logical size = '):
                # Split the line by spaces
                line_split = line.split(' ')
                # The last element of the split line is the grid size
                grid_size = float(line_split[-2])
            if line.startswith('Performed ') and 'iterations' in line:
                # Split the line by spaces
                line_split = line.split(' ')
                # The second element of the split line is the number of iterations
                num_iterations = int(line_split[1])
            if line.startswith('Total time: '):
                # Split the line by spaces
                line_split = line.split(' ')
                # The third element of the split line is the total time
                total_time = float(line_split[2])
            if line.startswith('Subgrid number per dimension = '):
                # Split the line by spaces
                line_split = line.split(' ')
                # The last element of the split line is the subgrid_num_z, but is equal to subgrid_num_x in our case
                subgrid_num_x = int(line_split[-1])

        gb_processed_per_second = (grid_size/1024)*num_iterations/total_time*2


    print(f'Experiment {i}:')
    
    if i >= 1000 and i <= 1999:
        print(f'Compression type: None')
    elif i >= 2000 and i <= 2999:
        print(f'Compression type: Shared')
    elif i >= 3000 and i <= 3999:
        print(f'Compression type: Global')
    elif i >= 4000 and i <= 4999:
        print(f'Compression type: None, but subgrids')
    else:
        print(f'Compression type: Unknown')

    print(f'Total grid logical size: {grid_size}')
    print(f'Number of iterations: {num_iterations}')
    print(f'Total time: {total_time}')
    print(f'GB processed per second: {gb_processed_per_second}')
    print(f'Subgrid number x: {subgrid_num_x}')

