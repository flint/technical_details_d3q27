import sys
import matplotlib.pyplot as plt
import numpy as np



# Read a file from args

# The file looks like this:
# Experiment 1000000:
# Scheduler: Heteroprio
# Total grid logical size: 23328.0
# Number of iterations: 10
# Total time: 2.713787
# GB processed per second: 167.89269017796903
# Experiment 1000001:
# Scheduler: Heteroprio
# Total grid logical size: 23328.0
# Number of iterations: 10
# Total time: 2.347415
# GB processed per second: 194.09648485674668
# Experiment 1000002:
# Scheduler: Heteroprio
# Total grid logical size: 23328.0
# Number of iterations: 10
# Total time: 2.358344
# GB processed per second: 193.197006034743
# .....

# This script will show box plots for the execution time of the different schedulers

# Read the file
if len(sys.argv) < 2:
    print('Usage: python3 plot_threshold_experiments.py <summary_file> [title]')
    sys.exit(1)

# We store the results in a nice structure that will make it easy to put them in a bar plot (a dictionary)

filename = sys.argv[1]

title = 'Execution times of a D3Q27 simulation with different schedulers'
if len(sys.argv) > 2:
    title = sys.argv[2]

# Array of results, will look like this: ['scheduler': ..., 'total_grid_logical_size': ..., 'num_iterations': ..., 'total_time': ..., 'gb_processed_per_second': ...]
results = []

# Read the file
with open(filename, 'r') as f:
    lines = f.readlines()

    # Iterate over the lines
    for line in lines:
        # If the line starts with "Experiment ", then we found the line we want
        if line.startswith('Experiment '):
            # Split the line by spaces
            line_split = line.split(' ')
            # The last element of the split line is the experiment number
            experiment_number = int(line_split[-1][:-2])

            # The next lines are the scheduler, the total grid logical size, the number of iterations, etc.
            scheduler = lines[lines.index(line) + 1].split(' ')[-1]
            total_grid_logical_size = float(lines[lines.index(line) + 2].split(' ')[-1])
            num_iterations = int(lines[lines.index(line) + 3].split(' ')[-1])
            total_time = float(lines[lines.index(line) + 4].split(' ')[-1])
            gb_processed_per_second = float(lines[lines.index(line) + 5].split(' ')[-1])

            results.append({'scheduler': scheduler, 'total_grid_logical_size': total_grid_logical_size, 'num_iterations': num_iterations, 'total_time': total_time, 'gb_processed_per_second': gb_processed_per_second})



# Now we can create the bar plots
# X axis: scheduler
# Y axis: execution time

# We will create a dictionary that will store the execution time for each scheduler
execution_time = {}

# Iterate over the results
for result in results:
    # If the scheduler is not in the dictionary, we create a new entry
    if result['scheduler'] not in execution_time:
        execution_time[result['scheduler']] = []
    # We append the execution time to the scheduler
    execution_time[result['scheduler']].append(result['total_time'])

# Now we can create the bar plot
fig, ax = plt.subplots(figsize=(9, 6))
# increase title size
ax.set_title(title)
ax.title.set_size(16)
ax.set_ylabel('Execution time (s)')
ax.set_xlabel('Scheduler')


# Create the box plot
ax.boxplot(execution_time.values())

# Set the labels
ax.set_xticklabels(execution_time.keys())

plt.show()

