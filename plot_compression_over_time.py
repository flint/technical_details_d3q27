import sys
import matplotlib.pyplot as plt
import numpy as np



# Read the result of an experiment and plot the compression over time

# We call: python3 plot_compression_over_time.py <experiment_folder>
# The script will read the file <experiment_folder>/output.txt, parse it, and plot the compression over time
# X axis: time
# Y axis: compression ratio

# Read the file
if len(sys.argv) < 2:
    print('Usage: python3 plot_compression_over_time.py <experiment_folder_1> <experiment_folder_2> ...')
    sys.exit(1)

data_points = [] # List of lists of tuples [[(time, compression_ratio), ...], [(time, compression_ratio), ...], ...]

# The target lines look like:
# end of iteration 3(t=0.0081): compression rate (real)=/245.885359(32390097696/131728452)
# end of iteration 3(t=0.0081): total size compressed=131728452
# end of iteration 4(t=0.0101): compression rate (real)=/245.719917(32390097696/131817144)
# end of iteration 4(t=0.0101): total size compressed=131817144
# end of iteration 5(t=0.0121): compression rate (real)=/245.480763(32390097696/131945564)
# end of iteration 5(t=0.0121): total size compressed=131945564
# end of iteration 6(t=0.0141): compression rate (real)=/245.162455(32390097696/132116876)

for filename in sys.argv[1:]:
    current_data_points = []
    filename = filename + '/output.txt'
    f = open(filename, 'r')

    with open(filename, 'r') as f:
        lines = f.readlines()

        # Iterate over the lines
        for line in lines:
            # If the line contains 'compression rate (real)', we parse it
            if 'compression rate (real)' in line:
                # Extract the time
                time = float((line.split('t=')[1]).split(')', 1)[0])
                # Extract the compression ratio
                print(line.split('=')[2]) # "/246.898688(32390097696/131187808)"
                compression_ratio = float((line.split('=')[2]).split('(', 1)[0][1:])
                # Add the data point to the list
                current_data_points.append((time, compression_ratio))
    data_points.append(current_data_points)

# # Sort the data points by time (just in case)
# data_points.sort(key=lambda x: x[0])

# # Print the data points
# for data_point in data_points:
#     print(data_point)

plot_grid_configurations = [#"Grid size: 693x2856x714 $\\tau_0=4\\times 10^{-8}$",
                            "Grid size: 594x2448x612",
                            "Grid size: 396x1632x408",
                            "Grid size: 198x816x204",
                            "Grid size: 165x680x170"]

# Reminder: data_points is a list of lists of tuples [[(time, compression_ratio), ...], [(time, compression_ratio), ...], ...]

# Plot the data points
x_1 = [x[0] for x in data_points[0]]
y_1 = [x[1] for x in data_points[0]]
if len(data_points) > 1:
    x_2 = [x[0] for x in data_points[1]]
    y_2 = [x[1] for x in data_points[1]]
if len(data_points) > 2:
    x_3 = [x[0] for x in data_points[2]]
    y_3 = [x[1] for x in data_points[2]]
if len(data_points) > 3:
    x_4 = [x[0] for x in data_points[3]]
    y_4 = [x[1] for x in data_points[3]]

plt.rcParams["figure.figsize"] = (12, 8)
ax = plt.gca()

ax.set_title('Compression ratio over time', fontsize=16)

plt.plot(x_1, y_1)
if len(data_points) > 1:
    plt.plot(x_2, y_2)
if len(data_points) > 2:
    plt.plot(x_3, y_3)
if len(data_points) > 3:
    plt.plot(x_4, y_4)
plt.xlabel('Time (simulated seconds)')
plt.ylabel('Compression ratio')

plt.yscale('log')
# plt.xscale('log')

# Show the labels
plt.legend(plot_grid_configurations)

plt.show()
